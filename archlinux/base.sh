#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - create xdg-runtime-dir
# - update image to latest packages
# - install dependencies that are likely needed in one or several projects
# - remove all cached packages to reduce the image size

mkdir -p $XDG_RUNTIME_DIR

pacman -Syu --quiet --noconfirm

# Notes:
# - This is a very minimal hand-picked set of packages.
# - Such that KDE Frameworks, Wrapland and up to KWinFT can be built with it.
# - Fat Boost lib in there is annoying to a certain degree. First time needed by KActivities.
# - Fat qt5-webkit is needed by KDEWebKit.
pacman -S --needed --quiet --noconfirm \
base-devel git pkgconf clang meson ninja cmake doxygen \
qt5-base qt5-script qt5-svg qt5-x11extras qt5-wayland qt5-multimedia qt5-xmlpatterns \
qt5-tools phonon-qt5 qt5-quickcontrols qt5-quickcontrols2 qt5-virtualkeyboard \
qt5-webkit upower networkmanager giflib shared-mime-info gperf boost lmdb \
qrencode libical docbook-xsl perl-uri \
xorg-server xorg-server-xvfb xorg-server-devel wayland-protocols \
xcursor-vanilla-dmz xcb-util-cursor xorg-server-xwayland mesa-demos \
python python-yaml python-lxml valgrind stress

pacman -Scc --noconfirm
