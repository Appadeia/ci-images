#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - clone FDBuild
# - build Wrapland
# - structure all dependencies of KWin by selecting KWin
# - exclude KDE Frameworks again (already included in the image)
# - exclude KWin
# - build all remaining dependencies (part of kde/workspace/)
# - build KWinFT
# - cleanup tooling and build files.

git clone --branch master https://gitlab.com/romangg/fdbuild.git /opt/fdbuild


# First build Wrapland.
mkdir -p /opt/wrapland
cd /opt/wrapland

printf "install:
  path: /usr
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
build:
  plugin: ninja
  threads: max
source:
  plugin: git
  origin: '%s'
  user: '%s'
  password: '%s'
  branch: master
  depth: 1
modules:
  - /\n" "https://${_GIT_HOST}/${_GIT_NAMESPACE}/wrapland.git" "$_GIT_USER" "$_GIT_PASSWORD" > fdbuild.yaml

/opt/fdbuild/fdbuild.py --noconfirm


# Now build additional dependencies.
mkdir -p /opt/kde-build && cd /opt/kde-build

printf "install:
  path: /usr
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
build:
  plugin: ninja
  threads: max
source:
  plugin: git
  branch: master
  depth: 1
structure:
  enabled: true
  plugin: kde
  branch-group: kf5-qt5
  flat: false
  selection:
  - kwin\n" > fdbuild.yaml

/opt/fdbuild/fdbuild.py --noconfirm --only-structure

cat fdbuild.yaml
cat kde/workspace/fdbuild.yaml

# Exclude KDE Frameworks again and KWin. Disable also the structure plugin
# so we don't rerun it again.
sed '/- frameworks/d' fdbuild.yaml > temp_fdbuild.yaml
sed 's/enabled\: true/enabled\: false/g' temp_fdbuild.yaml > fdbuild.yaml

sed '/- kwin/d' kde/workspace/fdbuild.yaml > temp_fdbuild.yaml
mv temp_fdbuild.yaml kde/workspace/fdbuild.yaml

cat fdbuild.yaml
cat kde/workspace/fdbuild.yaml

/opt/fdbuild/fdbuild.py --noconfirm


cd /opt
rm -rf /opt/wrapland
rm -rf /opt/kde-build
rm -rf /opt/fdbuild
