#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - clone FDBuild
# - build all KDE Frameworks from last tag
# - cleanup tooling and build files

git clone --branch master https://gitlab.com/romangg/fdbuild.git /opt/fdbuild

mkdir -p /opt/kde-build
cd /opt/kde-build

printf "install:
  path: /usr
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
build:
  plugin: ninja
  threads: max
source:
  plugin: git
  branch: master
  hooks:
    post: bash -c 'cd src && git checkout \$(git describe --abbrev=0 \$(git rev-list --tags --max-count=1)) && cd ..'
structure:
  plugin: kde
  branch-group: kf5-qt5
  flat: true
  selection:
  - frameworks\n" > fdbuild.yaml

/opt/fdbuild/fdbuild.py --noconfirm


cd /opt
rm -rf /opt/kde-build
rm -rf /opt/fdbuild
