# CI Image Bench

[![pipeline status][pipe-status-img]][pipe-status-link]

Docker Images for KWinFT projects CI are created here on a regular basis. The following images
are currently available:

## archlinux
Arch Linux is the **main** build and test OS.

Projects are built and tested on one of the following images in scheduled intervals.
Additionally on direct push to a branch the HEAD of the branch and every merge request's HEAD
shall be tested on a project level.

### base
Preinstalls dependencies and auxillary tools needed by most if not all projects.

Scheduled for recreation once a month.

### frameworks-master
Precompiles and installs KDE Frameworks from master branch.

Scheduled for recreation every two weeks.

### frameworks-stable
Precompiles and installs KDE Frameworks from last stable release tag.

Will be rebuilt on downstream tag build.

### kwinft-base-master
Precompiles and installs additional packages from kde/workspace that are needed for the compilation
and execution of KWinFT. Includes also Wrapland from its master branch.

### kwinft-base-stable
Precompiles and installs additional packages from kde/workspace that are needed for the compilation
and execution of KWinFT. Includes also Wrapland from its current stable branch.

[pipe-status-img]: https://gitlab.com/kwinft/ci-images/badges/master/pipeline.svg
[pipe-status-link]: https://gitlab.com/kwinft/ci-images/-/commits/master
