#!/bin/bash

set -ex
set -o pipefail

echo $PLASMA_VERSION

# Install necessary tooling.
yum -y install buildah runc

# Make sure mountopts are unset. Newer versions of buildah set these to values
# that fail with the vfs driver.
# See:
# - https://github.com/containers/buildah/issues/1783
# - https://major.io/2019/08/13/buildah-error-vfs-driver-does-not-support-overlay-mountopt-options
sed -i '/^mountopt =.*/d' /etc/containers/storage.conf || true

# Login and get the image.
buildah login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
ID=$(buildah from ${IMAGE_BASE})

# Read in optional environment variables.
# We unset the x option so we don't spam the log.
ENV_FILE=${IMAGE_OS}/${IMAGE_TARGET}-env
set +x
if [ -f "$ENV_FILE" ]
then
    while read FULL_LINE
    do
        LINE="$(echo -e "$FULL_LINE" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"
        if [ -z "$LINE" ]; then
            continue
        fi
        if [[ $LINE == //* ]]; then
            continue
        fi
        buildah config --env $LINE $ID
        echo "Added environment variable: ${LINE}"
    done < $ENV_FILE
fi
set -x

# Set some temporary environment variables that allow to build in a private project.
buildah config --env _GIT_USER=${CI_REGISTRY_USER} --env _GIT_PASSWORD=${CI_REGISTRY_PASSWORD} \
    --env _GIT_HOST=${CI_SERVER_HOST} --env _GIT_NAMESPACE=${CI_PROJECT_NAMESPACE} $ID

# Unconditionally set the PLASMA_VERSION variable. In stable builds it is set, otherwise it is just empty. TODO
if [ -n "$PLASMA_VERSION" ]; then
    buildah config --env PLASMA_VERSION=${PLASMA_VERSION} $ID
fi

# Add the main build script to the container, run it and remove it again.
buildah copy $ID ${IMAGE_OS}/${IMAGE_TARGET}.sh /opt/buildah.sh
buildah run $ID bash /opt/buildah.sh
buildah run $ID rm /opt/buildah.sh

# Unset the temporary environment variables again.
buildah config --env _GIT_USER- --env _GIT_PW- --env _GIT_HOST- --env _GIT_NAMESPACE- $ID

# Commit the image and push one latest and one date-tagged version.
buildah commit $ID img

TODAY=$(date +'%Y-%m-%d')
buildah push img ${CI_REGISTRY_IMAGE}/${IMAGE_OS}/${IMAGE_TARGET}
buildah push img ${CI_REGISTRY_IMAGE}/${IMAGE_OS}/${IMAGE_TARGET}:${TODAY}
